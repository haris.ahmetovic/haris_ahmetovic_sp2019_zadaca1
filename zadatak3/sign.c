#include <stdio.h>
#include <signal.h>
#include <unistd.h>

int volatile num = 0;
int volatile quit = 0;

void mojpause() {
  alarm(1);
  while(!quit)
    ;
}

void quitHandler(int sig) {
  quit = 1;
}

void interuptHandler(int sig) {
  num++;
}

void alarmHandler(int sig) {
  write(STDOUT_FILENO, "Pokrenut sam...\n",16);
  alarm(3);
}

int main(int argc, char *argv[]) {
  signal(SIGALRM, alarmHandler);
  signal(SIGINT, interuptHandler);
  signal(SIGQUIT, quitHandler);

  mojpause();

  printf("Signal SIGINT sam dobio: %d puta\n", num);
  return 0;

}
