#include <termios.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#define UP "\033[A"
#define DOWN "\033[B"
#define RIGHT "\033[C"
#define LEFT "\033[D"
#define ESC 27
#define BACKSPACE 8
#define INS 45
#define DELETE 127
#define DEL "\b\033[P"

enum TerminalModes {
    INSERT,
    REPLACE,
    COMMAND
};

struct termios orig_termios;
int ttyfd = STDIN_FILENO;

void fatal(char *message) {
    perror(message);
    exit(1);
}

int tty_reset() {
    if (tcsetattr(ttyfd,TCSAFLUSH,&orig_termios) < 0)
        return -1;
    return 0;
}

void tty_atexit() {
    tty_reset();
}

void set_tty() {
    struct termios raw;
    raw = orig_termios;
    raw.c_lflag &= ~(ECHO | ICANON | ISIG);
    raw.c_cc[VMIN] = 0;
    raw.c_cc[VTIME] = 10;
    if (tcsetattr(ttyfd,TCSAFLUSH,&raw) < 0)
        fatal("can't set raw mode");
}
   
int process_term() {
    int n;

    char c_in, c_out;
    char insertChar[10];
    char clearDisplay[10];

    enum TerminalModes terminalMode = COMMAND;

    sprintf(clearDisplay, "\033[2J");
    write(STDOUT_FILENO, clearDisplay, 4);
    write(STDOUT_FILENO, "\033[1H", 4);

    while(1) {
        n = read( ttyfd, &c_in, 1  );
        if (n < 0)
            fatal("read error");
        if (n == 0) {
        }
        else {
            switch (terminalMode){
                case INSERT:
                    switch (c_in) {
                        case ESC: 
                            terminalMode = COMMAND;
                            break;
                        
                        default: 
                            sprintf(insertChar, "\033[@%c", c_in);
                            write(STDOUT_FILENO, insertChar, 4);
                    }
                    break;
                case REPLACE:
                    switch (c_in) {
                        case ESC: 
                            terminalMode = COMMAND;
                            break;
                        case BACKSPACE: 
                        case DELETE:
                            write(STDOUT_FILENO, DEL, 4);
                            break;
                        default:
                            write(STDOUT_FILENO, &c_in, 1);
                    }
                    break; 
                case COMMAND:
                    switch (c_in) {
                        case 'I':
                        case 'i':
                        case INS:
                            terminalMode = INSERT;
                            break;
                        case 'R': 
                        case 'r': 
                            terminalMode = REPLACE;
                            break;
                        case 'Q':
                        case 'q':
                            return 0;
                        case 'K':
                        case 'k': 
                            write(STDOUT_FILENO, UP, 3);
                            break;
                        case 'J':
                        case 'j': 
                            write(STDOUT_FILENO, DOWN, 3);
                            break;
                        case 'L':
                        case 'l': 
                            write(STDOUT_FILENO, RIGHT, 3);
                            break;
                        case 'H':
                        case 'h': 
                            write(STDOUT_FILENO, LEFT, 3);
                            break; 
                    }
                    break;
            }
        }
    }
}

int main() {
    if (!isatty(ttyfd))
        fatal("not on a tty");

    if (tcgetattr(ttyfd,&orig_termios) < 0)
        fatal("can't get tty settings");

    if (atexit(tty_atexit) != 0)
        fatal("atexit: can't register tty reset");

    set_tty();

    process_term();
    return 0;
}
