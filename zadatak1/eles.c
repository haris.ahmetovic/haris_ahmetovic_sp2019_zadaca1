#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <pwd.h>
#include <time.h>
#include <grp.h>

void printInodePermissions(struct stat* statInfo){
    printf( (statInfo->st_mode & S_IRUSR) ? "r" : "-");
    printf( (statInfo->st_mode & S_IWUSR) ? "w" : "-");
    printf( (statInfo->st_mode & S_IXUSR) ? "x" : "-");
    printf( (statInfo->st_mode & S_IRGRP) ? "r" : "-");
    printf( (statInfo->st_mode & S_IWGRP) ? "w" : "-");
    printf( (statInfo->st_mode & S_IXGRP) ? "x" : "-");
    printf( (statInfo->st_mode & S_IROTH) ? "r" : "-");
    printf( (statInfo->st_mode & S_IWOTH) ? "w" : "-");
    printf( (statInfo->st_mode & S_IXOTH) ? "x" : "-");
}

void printInodeType(struct stat* statInfo){
    switch (statInfo->st_mode & S_IFMT) {
        case S_IFBLK:  printf("b"); break;
        case S_IFCHR:  printf("c"); break;
        case S_IFDIR:  printf("d"); break;
        case S_IFIFO:  printf("p"); break;
        case S_IFLNK:  printf("l"); break;
        case S_IFSOCK: printf("s"); break;
        default:
            printf("-"); break;
    }
}

void printInfo(struct stat* statInfo, char const* inodeName){
    char* userName = getpwuid(statInfo->st_uid)->pw_name;
    char* groupName = getgrgid(statInfo->st_gid)->gr_name;

    char timeOfLastMod[16];
    strftime(timeOfLastMod, 16, "%Y-%m-%d %H:%M", localtime(&statInfo->st_mtime));

    printf("%7lu ",statInfo->st_ino);
    printInodeType(statInfo);
    printInodePermissions(statInfo);
    printf(" %3lu", statInfo->st_nlink);
    printf(" %d(%s) %d(%s)", statInfo->st_uid, userName, statInfo->st_gid, groupName);
    printf(" %10li", statInfo->st_size);
    printf(" %16s", timeOfLastMod);
    printf(" %s\n", inodeName);
}

void parseDir(const char* path){
    DIR* dirStream;
  
    if(!(dirStream = opendir(path))){
      printf("mojls: %s\n", strerror(errno));
      return;
    }
  
    struct dirent* nextDirEntry;
    struct stat inodeInfo;
    char * pathName[256];
  
    while((nextDirEntry = readdir(dirStream)) != NULL){
     stat(nextDirEntry->d_name, &inodeInfo);
     printInfo(&inodeInfo, nextDirEntry->d_name );
    }
  
}



void parsePaths(const char* validPathEntries[], int numOfEntries){
  struct stat info;

  for(int i = 0; i < numOfEntries; i++){
    stat(validPathEntries[i], &info);
    
    if(S_ISDIR(info.st_mode))
    {
      parseDir(validPathEntries[i]);
    }
    else if(S_ISREG(info.st_mode))
    {
      printInfo(&info, validPathEntries[i]);
    }
  }  

}

void filterPaths(char* entry[], int n){
    struct stat fileInfo;
    int k = 0;
    const char* validPath[20];
  
    for(int i = 1; i < n; ++i){
        if((stat(entry[i], &fileInfo)) < 0){
          printf("mojls: cannot access %s: %s\n", entry[i], strerror(errno));
        }
        else{
          validPath[k++] = entry[i];
        }
    }
    parsePaths(validPath, k);
}

int main(int argc, char* argv[]){
  if(argc == 1){
      parseDir(".");
  }
  else{
      filterPaths(argv, argc);
    }

  return 0;
}


